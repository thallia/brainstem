# brainSTEM

This project is for the purpose of having a mindmap like database of medical information--from eastern/natural to western/pharma research, documents, and information. (including recipes, treatments, other sources of information like WebMD, openstax textbooks, etc)

Inspiration comes from:
* https://www.sixdegreesofwikipedia.com/
* https://wiki.nikitavoloboev.xyz/macos/macos-apps/mindnode
* https://learn-anything.xyz

The UI/UX I want to work similarly to Learn Anything - just a search bar, and something that brings up multiple results that are connected to each other. Everything that's related will be by keyword or common topic (like six degrees of wikipedia).

Everything will have tags based on what it's talking about, and those will relate everything to each other, offering suggested other topics/research/things to read that are related to the subject you're looking at.

I would preferably like if all the resources were downloaded and accessed locally as PDFs or other files, that way you prevent the loss of access to a website or if a website moves/shuts down. (Or at least have the option to either download it from the server or view it on the site)

Main language to be working in is python.

Sample MariaDB Table:

| Index | Article Title | Date Published | Author ID | Publisher ID | DOI Number | Keywords | Tags | PDF File | Abstract | Journal ID | Volume |
| :--   | :--           | :--            | :--       | :--          | :--        | :--      | :--  | :--      | :--      | :--        | :--    |
| index the entries with ints | title of paper | date/timestamp | array of author IDs that point to another author table | publisher id that points to other table with publishers | floating point number probably | do we really need this? | scrape buzzwords and assign an array of IDs from another table that point to a list of tags | actual link or maybe the download of the file | /shrug | ID that points to a list of journals | int |

![brainSTEM database flowchart](brainSTEM_flow.png)

- **pink:** name of the database table
- **turquoise:** columns in the table
- **orange:** datatype of the entry


Setup Ideas:
* MySQL database (using MariaDB)
* Setup Syncthing between local hard drive and VPS (when eventually deployed on a server)
* Use python & APIs from the large research websites
* Use [figma](https://www.figma.com/) to set up and visualize the UI.
* Use machine learning to introduce and apply statistics to relate to certain research papers and potentially (maybe have a plugin for) patient/medical data that relates and organizes things by disease/study and applies them mind-mappingly to the research papers as well, so along with the research you can see the current data/statistics that relate to that problem/health status/cure.

MariaDB: https://github.com/MariaDB/server

Syncthing: https://syncthing.net

APIs from larger websites (will collect more as we actually get this working):
* Elsevier ScienceDirect - https://dev.elsevier.com/sd_apis.html
* Python Module for Elsevier API usage - https://github.com/ElsevierDev/elsapy
* PubMed Central (PMC, NCBI) - https://www.ncbi.nlm.nih.gov/pmc/tools/developers/

Potential Roles (if this project goes anywhere):
* Project lead -- thallia
* API developers -- Trevor
* UI/UX developers --
* Server / MySQL / MariaDB admin(s) -- thallia
